/* eslint-disable no-underscore-dangle */
import { ThunkAction } from 'redux-thunk';
import axios from 'axios';
import serverComm from '../../services/comms/boardService';
import cUtils from '../../services/kanbanlogic/cardUtils';
import clUtils from '../../services/kanbanlogic/cardListUtils';
import { RootReducerState } from '../../types/redux/reducers';
import { CardData } from '../../types/kanban/cards';
import {
  ChangeErrorAction, CHANGE_CURRENT_BOARD, KanbanActionTypes, UPDATE_BOARD, ViewActionTypes,
} from '../../types/redux/actions';
import isError from '../../services/validation/error';
import changeError from '../errors/errorActions';


export const addCard = (
  listId: string,
  card: CardData,
): ThunkAction<void, RootReducerState, unknown, KanbanActionTypes | ChangeErrorAction | ViewActionTypes> => {
  return async (dispatch, getState) => {
    try {

      const token = getState().userstate.user.usertoken;
      if (!token) throw new Error('No user token for operation');

      const targetBoard = getState().boardstate.boards.find(b => b.lists.find(l => l._id === listId));
      if (!targetBoard) throw new Error('Tried to add card to unknown list');

      const targetList = targetBoard.lists.find(l => l._id === listId);
      if (!targetList) throw new Error('Unexpected exception');

      const updatedList = cUtils.addCardToList(card, targetList);
      const updatedBoard = clUtils.updateListInBoard(updatedList, targetBoard);

      dispatch({ type: UPDATE_BOARD, payload: { board: updatedBoard } });
      dispatch({ type: CHANGE_CURRENT_BOARD, payload: { board: updatedBoard } });

      await serverComm.updateBoardOnline(updatedBoard, token);


    } catch (error) {
      if (axios.isAxiosError(error)) {
        console.error(error);
        dispatch(changeError(error.response?.data.message));
        setTimeout(() => dispatch(changeError(null)), 2000);
      } else if (isError(error)) {
        console.error(error);
        dispatch(changeError(error.message));
        setTimeout(() => dispatch(changeError(null)), 2000);
      }
    }


  };
};

export const deleteCard = (
  listId: string,
  card: CardData,
): ThunkAction<void, RootReducerState, unknown, KanbanActionTypes | ChangeErrorAction | ViewActionTypes> => {
  return async (dispatch, getState) => {
    try {

      const token = getState().userstate.user.usertoken;
      if (!token) throw new Error('No user token for operation');

      const targetBoard = getState().boardstate.boards.find(b => b.lists.find(l => l._id === listId));
      if (!targetBoard) throw new Error('Tried to delete card from unknown list');

      const targetList = targetBoard.lists.find(l => l._id === listId);
      if (!targetList) throw new Error('Unexpected exception');

      const updatedList = cUtils.removeCardFromList(card, targetList);
      const updatedBoard = clUtils.updateListInBoard(updatedList, targetBoard);

      dispatch({ type: UPDATE_BOARD, payload: { board: updatedBoard } });
      dispatch({ type: CHANGE_CURRENT_BOARD, payload: { board: updatedBoard } });

      await serverComm.updateBoardOnline(updatedBoard, token);

    } catch (error) {
      if (axios.isAxiosError(error)) {
        console.error(error);
        dispatch(changeError(error.response?.data.message));
        setTimeout(() => dispatch(changeError(null)), 2000);
      } else if (isError(error)) {
        console.error(error);
        dispatch(changeError(error.message));
        setTimeout(() => dispatch(changeError(null)), 2000);
      }
    }


  };
};

export const updateCard = (
  listId: string,
  card: CardData,
): ThunkAction<void, RootReducerState, unknown, KanbanActionTypes | ChangeErrorAction | ViewActionTypes> => {
  return async (dispatch, getState) => {
    try {

      const token = getState().userstate.user.usertoken;
      if (!token) throw new Error('No user token for operation');

      const targetBoard = getState().boardstate.boards.find(b => b.lists.find(l => l._id === listId));
      if (!targetBoard) throw new Error('Tried to update card in unknown list');

      const targetList = targetBoard.lists.find(l => l._id === listId);
      if (!targetList) throw new Error('Unexpected exception');

      const updatedList = cUtils.updateCardInList(card, targetList);
      const updatedBoard = clUtils.updateListInBoard(updatedList, targetBoard);

      dispatch({ type: UPDATE_BOARD, payload: { board: updatedBoard } });
      dispatch({ type: CHANGE_CURRENT_BOARD, payload: { board: updatedBoard } });

      await serverComm.updateBoardOnline(updatedBoard, token);


    } catch (error) {
      if (axios.isAxiosError(error)) {
        console.error(error);
        dispatch(changeError(error.response?.data.message));
        setTimeout(() => dispatch(changeError(null)), 2000);
      } else if (isError(error)) {
        console.error(error);
        dispatch(changeError(error.message));
        setTimeout(() => dispatch(changeError(null)), 2000);
      }
    }
  };
};

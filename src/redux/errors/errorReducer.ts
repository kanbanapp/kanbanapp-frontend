/* eslint-disable linebreak-style */

import { ChangeErrorAction, CHANGE_ERROR } from '../../types/redux/actions';
import { ErrorReducerState } from '../../types/redux/reducers';


const initialState: ErrorReducerState = {
  error: null,
};

const errorReducer = (state = initialState, action: ChangeErrorAction) => {

  switch (action.type) {

    case CHANGE_ERROR: {
      return action.payload;
    }

    default: return state;

  }

};

export default errorReducer;

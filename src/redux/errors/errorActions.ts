import { ChangeErrorAction, CHANGE_ERROR } from '../../types/redux/actions';

const changeError = (error: string|null): ChangeErrorAction => {
  return { type: CHANGE_ERROR, payload: { error } };
};

export default changeError;

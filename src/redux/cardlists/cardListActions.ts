/* eslint-disable no-underscore-dangle */
import { ThunkAction } from 'redux-thunk';
import axios from 'axios';
import { RootReducerState } from '../../types/redux/reducers';
import serverComm from '../../services/comms/boardService';
import clUtils from '../../services/kanbanlogic/cardListUtils';
import { CardListData } from '../../types/kanban/cardlists';
import {
  ChangeErrorAction, CHANGE_CURRENT_BOARD, KanbanActionTypes, UPDATE_BOARD, ViewActionTypes,
} from '../../types/redux/actions';
import isError from '../../services/validation/error';
import changeError from '../errors/errorActions';



export const addCardList = (
  boardId: string,
  cardList: CardListData,
): ThunkAction<void, RootReducerState, unknown, KanbanActionTypes | ChangeErrorAction | ViewActionTypes> => {
  return async (dispatch, getState) => {
    try {

      const token = getState().userstate.user.usertoken;
      if (!token) throw new Error('No user token for operation');

      const targetBoard = getState().boardstate.boards.find(b => b._id === boardId);
      if (!targetBoard) throw new Error('Tried to add cardlist to unknown board');

      const updatedBoard = clUtils.addCardList(cardList, targetBoard);
      console.log(updatedBoard);

      dispatch({ type: UPDATE_BOARD, payload: { board: updatedBoard } });
      dispatch({ type: CHANGE_CURRENT_BOARD, payload: { board: updatedBoard } });

      await serverComm.updateBoardOnline(updatedBoard, token);

    } catch (error) {
      if (axios.isAxiosError(error)) {
        console.error(error);
        dispatch(changeError(error.response?.data.message));
        setTimeout(() => dispatch(changeError(null)), 2000);
      } else if (isError(error)) {
        console.error(error);
        dispatch(changeError(error.message));
        setTimeout(() => dispatch(changeError(null)), 2000);
      }
    }
  };
};

export const deleteCardList = (
  boardId: string,
  cardList: CardListData,
): ThunkAction<void, RootReducerState, unknown, KanbanActionTypes | ChangeErrorAction | ViewActionTypes> => {
  return async (dispatch, getState) => {
    try {

      const token = getState().userstate.user.usertoken;
      if (!token) throw new Error('No user token for operation');

      const targetBoard = getState().boardstate.boards.find(b => b._id === boardId);
      if (!targetBoard) throw new Error('Tried to delete cardlist from unknown board');

      const updatedBoard = clUtils.removeCardList(cardList, targetBoard);

      dispatch({ type: UPDATE_BOARD, payload: { board: updatedBoard } });
      dispatch({ type: CHANGE_CURRENT_BOARD, payload: { board: updatedBoard } });

      await serverComm.updateBoardOnline(updatedBoard, token);

    } catch (error) {
      if (axios.isAxiosError(error)) {
        console.error(error);
        dispatch(changeError(error.response?.data.message));
        setTimeout(() => dispatch(changeError(null)), 2000);
      } else if (isError(error)) {
        console.error(error);
        dispatch(changeError(error.message));
        setTimeout(() => dispatch(changeError(null)), 2000);
      }
    }
  };
};

export const updateCardList = (
  boardId: string,
  cardList: CardListData,
): ThunkAction<void, RootReducerState, unknown, KanbanActionTypes | ChangeErrorAction | ViewActionTypes> => {
  return async (dispatch, getState) => {
    try {

      const token = getState().userstate.user.usertoken;
      if (!token) throw new Error('No user token for operation');

      const targetBoard = getState().boardstate.boards.find(b => b._id === boardId);
      if (!targetBoard) throw new Error('Tried to update cardlist in unknown board');

      const updatedBoard = clUtils.updateListInBoard(cardList, targetBoard);

      dispatch({ type: UPDATE_BOARD, payload: { board: updatedBoard } });
      dispatch({ type: CHANGE_CURRENT_BOARD, payload: { board: updatedBoard } });

      await serverComm.updateBoardOnline(updatedBoard, token);

    } catch (error) {
      if (axios.isAxiosError(error)) {
        console.error(error);
        dispatch(changeError(error.response?.data.message));
        setTimeout(() => dispatch(changeError(null)), 2000);
      } else if (isError(error)) {
        console.error(error);
        dispatch(changeError(error.message));
        setTimeout(() => dispatch(changeError(null)), 2000);
      }
    }
  };
};

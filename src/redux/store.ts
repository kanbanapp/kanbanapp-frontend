import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import boardReducer from './boards/boardReducer';
import userReducer from './users/userReducer';
import errorReducer from './errors/errorReducer';
import viewReducer from './views/viewReducer';

const rootReducer = combineReducers({
  userstate: userReducer,
  boardstate: boardReducer,
  errorstate: errorReducer,
  viewstate: viewReducer,
});

const store = createStore(
    rootReducer as any,
    composeWithDevTools(applyMiddleware(thunk)),
);

export default store;

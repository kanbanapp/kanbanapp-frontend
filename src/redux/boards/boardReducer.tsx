/* eslint-disable no-underscore-dangle */
/* eslint-disable no-param-reassign */


import boardUtils from '../../services/kanbanlogic/boardUtils';
import {
  KanbanActionTypes, ADD_BOARD, DELETE_BOARD, UPDATE_BOARD, CHANGE_BOARD_STATUS, INIT_BOARDS, CLEAR_BOARDS,
} from '../../types/redux/actions';
import { BoardReducerState } from '../../types/redux/reducers';
import RequestState from '../../types/requeststate';

const initState: BoardReducerState = {
  boards: [],
  status: RequestState.IDLE,
};

const boardReducer = (
  state: BoardReducerState = initState,
  action: KanbanActionTypes,
) => {
  switch (action.type) {
    case ADD_BOARD: {
      const newState = boardUtils.addBoard(action.payload.board, state.boards);
      return { ...state, boards: newState };
    }

    case DELETE_BOARD: {
      const newState = boardUtils.removeBoard(action.payload.board, state.boards);
      return { ...state, boards: newState };
    }

    case UPDATE_BOARD: {
      const newState = boardUtils.updateBoard(action.payload.board, state.boards);
      return { ...state, boards: newState };
    }

    case CHANGE_BOARD_STATUS: {
      return { ...state, status: action.payload.status };
    }

    case INIT_BOARDS: {
      return { ...state, boards: action.payload.boards };
    }

    case CLEAR_BOARDS: {
      return { initState };
    }

    default: return state;
  }
};

export default boardReducer;

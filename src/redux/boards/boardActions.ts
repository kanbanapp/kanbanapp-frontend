import axios from 'axios';
import { ThunkAction } from 'redux-thunk';

import serverComm from '../../services/comms/boardService';
import isError from '../../services/validation/error';
import { isBoardArray } from '../../services/validation/kanban';
import { BoardData } from '../../types/kanban/boards';
import {
  KanbanActionTypes, ADD_BOARD, DELETE_BOARD, UPDATE_BOARD, CHANGE_BOARD_STATUS, INIT_BOARDS, ChangeErrorAction, ViewActionTypes, CHANGE_CURRENT_BOARD, CLEAR_BOARDS,
} from '../../types/redux/actions';
import { RootReducerState } from '../../types/redux/reducers';
import RequestState from '../../types/requeststate';
import changeError from '../errors/errorActions';

export const addBoard = (
  board: BoardData,
): ThunkAction<void, RootReducerState, unknown, KanbanActionTypes|ChangeErrorAction> => {
  return async (dispatch, getState) => {
    try {

      const token = getState().userstate.user.usertoken;
      if (!token) throw new Error('No user token for operation');

      await serverComm.createBoardOnline(board, token);

      dispatch({ type: ADD_BOARD, payload: { board } });

    } catch (error) {

      if (axios.isAxiosError(error)) {
        console.error(error);
        dispatch(changeError(error.response?.data.message));
        setTimeout(() => dispatch(changeError(null)), 2000);
      } else if (isError(error)) {
        console.error(error);
        dispatch(changeError(error.message));
        setTimeout(() => dispatch(changeError(null)), 2000);
      }

    }
  };
};

export const deleteBoard = (
  board: BoardData,
): ThunkAction<void, RootReducerState, unknown, KanbanActionTypes | ChangeErrorAction> => {
  return async (dispatch, getState) => {
    try {

      const token = getState().userstate.user.usertoken;
      if (!token) throw new Error('No user token for operation');

      await serverComm.deleteBoardOnline(board, token);

      dispatch({ type: DELETE_BOARD, payload: { board } });

    } catch (error) {

      if (axios.isAxiosError(error)) {
        console.error(error);
        dispatch(changeError(error.response?.data.message));
        setTimeout(() => dispatch(changeError(null)), 2000);
      } else if (isError(error)) {
        console.error(error);
        dispatch(changeError(error.message));
        setTimeout(() => dispatch(changeError(null)), 2000);
      }

    }
  };
};

export const updateBoard = (
  board: BoardData,
): ThunkAction<void, RootReducerState, unknown, KanbanActionTypes | ChangeErrorAction | ViewActionTypes> => {
  return async (dispatch, getState) => {
    try {

      const token = getState().userstate.user.usertoken;
      if (!token) throw new Error('No user token for operation');

      dispatch({ type: UPDATE_BOARD, payload: { board } });
      dispatch({ type: CHANGE_CURRENT_BOARD, payload: { board } });

      await serverComm.updateBoardOnline(board, token);

    } catch (error) {

      if (axios.isAxiosError(error)) {
        console.error(error);
        dispatch(changeError(error.response?.data.message));
        setTimeout(() => dispatch(changeError(null)), 2000);
      } else if (isError(error)) {
        console.error(error);
        dispatch(changeError(error.message));
        setTimeout(() => dispatch(changeError(null)), 2000);
      }

    }
  };
};

export const initBoards = (): ThunkAction<void, RootReducerState, unknown, KanbanActionTypes | ChangeErrorAction> => {
  return async (dispatch, getState) => {
    try {

      const token = getState().userstate.user.usertoken;
      if (!token) throw new Error('No user token for operation');

      const boards = await serverComm.getAllUserBoardsOnline(token);
      if (!isBoardArray(boards)) throw new Error('Malformed server response');

      dispatch({ type: INIT_BOARDS, payload: { boards } });

    } catch (error) {

      if (axios.isAxiosError(error)) {
        console.error(error);
        dispatch(changeError(error.response?.data.message));
        setTimeout(() => dispatch(changeError(null)), 2000);
      } else if (isError(error)) {
        console.error(error);
        dispatch(changeError(error.message));
        setTimeout(() => dispatch(changeError(null)), 2000);
      }
    }
  };
};

export const changeBoardRequestStatus = (newStatus: RequestState): KanbanActionTypes => {
  return { type: CHANGE_BOARD_STATUS, payload: { status: newStatus } };
};

export const clearBoards = (): KanbanActionTypes => {
  return { type: CLEAR_BOARDS };
};

import AppView from '../../types/appview';
import { BoardData } from '../../types/kanban/boards';
import { CardData } from '../../types/kanban/cards';
import {
  ChangeCurrentBoardAction, ChangeCurrentCardAction, ChangeViewAction, CHANGE_CURRENT_BOARD, CHANGE_CURRENT_CARD, CHANGE_VIEW,
} from '../../types/redux/actions';


export const changeView = (view: AppView): ChangeViewAction => {
  return { type: CHANGE_VIEW, payload: { view } };
};

export const changeCurrentBoard = (board: BoardData|null): ChangeCurrentBoardAction => {
  return { type: CHANGE_CURRENT_BOARD, payload: { board } };
};

export const changeCurrentCard = (card: CardData|null): ChangeCurrentCardAction => {
  return { type: CHANGE_CURRENT_CARD, payload: { card } };
};



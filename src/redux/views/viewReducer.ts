/* eslint-disable linebreak-style */

import AppView from '../../types/appview';
import {
  CHANGE_CURRENT_BOARD, CHANGE_CURRENT_CARD, CHANGE_VIEW, ViewActionTypes,
} from '../../types/redux/actions';
import { ViewReducerState } from '../../types/redux/reducers';

const initialState: ViewReducerState = {
  view: AppView.WELCOMEVIEW,
  currentBoard: null,
  currentCard: null,
};

const viewReducer = (state = initialState, action: ViewActionTypes) => {

  switch (action.type) {

    case CHANGE_VIEW: {
      return { ...state, view: action.payload.view };
    }

    case CHANGE_CURRENT_BOARD: {
      return { ...state, currentBoard: action.payload.board };
    }

    case CHANGE_CURRENT_CARD: {
      return { ...state, currentCard: action.payload.card };
    }

    default: return state;

  }

};

export default viewReducer;

/* eslint-disable linebreak-style */

import { LoginUser } from '../../types/kanban/users';
import {
  UserActionTypes, SIGNUP_USER, LOGIN_USER, LOGOUT_USER, UPDATE_USER, CHANGE_USER_STATUS,
} from '../../types/redux/actions';
import { UserReducerState } from '../../types/redux/reducers';
import RequestState from '../../types/requeststate';


const defUser: LoginUser = {
  _id: 'Guest',
  username: 'Guest',
  passwordhash: '',
  boards: [],
  settings: [],
  usertoken: undefined,
};

const initialState: UserReducerState = {
  user: defUser,
  status: RequestState.IDLE,
};

const userReducer = (state = initialState, action: UserActionTypes) => {
  switch (action.type) {

    case SIGNUP_USER: {
      return defUser;
    }

    case LOGIN_USER: {
      return { ...state, user: action.payload };
    }

    case LOGOUT_USER: {
      return { ...state, user: defUser };
    }

    case UPDATE_USER: {
      return ({ ...state, user: { ...action.payload.user, usertoken: state.user.usertoken } });
    }

    case CHANGE_USER_STATUS: {
      return { ...state, status: action.payload.status };
    }

    default: return state;
  }
};

export default userReducer;

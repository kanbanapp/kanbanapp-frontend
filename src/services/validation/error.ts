const isError = (o: any): o is Error => {
  return (o.name && o.message && typeof o.name === 'string' && typeof o.message === 'string');
};

export default isError;

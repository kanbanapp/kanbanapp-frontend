import axios from 'axios';
import { NewUser } from '../../types/kanban/users';


const baseUrl = `${process.env.REACT_APP_SERVERURL}/login`;

// User login
const login = (userData: NewUser) => {
  return (axios.post(`${baseUrl}`, userData))
    .then(response => response.data);
};

export default { login };

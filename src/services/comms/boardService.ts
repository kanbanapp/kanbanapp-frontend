/* eslint-disable no-underscore-dangle */
import axios from 'axios';
import { NewBoardData, BoardData } from '../../types/kanban/boards';

const baseUrl = `${process.env.REACT_APP_SERVERURL}/boards`;


// Board fetch
const getBoardOnline = (id: string, userToken: string) => {
  return (axios.get(`${baseUrl}/${id}`, { headers: { authorization: userToken } }))
    .then(response => response.data);
};

const getAllUserBoardsOnline = (userToken: string) => {
  return (axios.get(baseUrl, { headers: { authorization: userToken } }))
    .then(response => response.data);
};

// Board creation
const createBoardOnline = (newBoard: NewBoardData, userToken: string) => {
  return (axios.post(baseUrl, newBoard, { headers: { authorization: userToken } })
    .then(response => response.data));
};

// Board update
const updateBoardOnline = (board: BoardData, userToken: string) => {
  return (axios.put(`${baseUrl}/${board._id}`, board, { headers: { authorization: userToken } })
    .then(response => response.data));
};

// Board deletion
const deleteBoardOnline = (board: BoardData, userToken: string) => {
  return (axios.delete(`${baseUrl}/${board._id}`, { headers: { authorization: userToken } })
    .then(response => response.data));
};

export default {
  getBoardOnline,
  getAllUserBoardsOnline,
  createBoardOnline,
  updateBoardOnline,
  deleteBoardOnline,
};

/* eslint-disable no-underscore-dangle */
import cloneDeep from 'lodash.clonedeep';
import { CardListData } from '../../types/kanban/cardlists';
import { CardData } from '../../types/kanban/cards';

const addCardToList = (card: CardData, list: CardListData) => {
  const cloneData = cloneDeep(list);
  cloneData.cards.push(card);
  return cloneData;
};

const removeCardFromList = (card: CardData, list: CardListData) => {
  const cloneData = cloneDeep(list);
  const cardIndex = cloneData.cards.findIndex(c => c._id === card._id);
  if (cardIndex === -1) throw new Error('Removable card did not exist in list.');
  cloneData.cards.splice(cardIndex, 1);
  return cloneData;
};

const updateCardInList = (card: CardData, list: CardListData) => {
  const cloneData = cloneDeep(list);
  const cardIndex = cloneData.cards.findIndex(c => c._id === card._id);
  if (cardIndex === -1) throw new Error('Updatable card did not exist in list.');
  cloneData.cards.splice(cardIndex, 1, card);
  return cloneData;
};

export default { addCardToList, removeCardFromList, updateCardInList };

/* eslint-disable no-underscore-dangle */
import cloneDeep from 'lodash.clonedeep';
import { BoardData } from '../../types/kanban/boards';
import { CardListData } from '../../types/kanban/cardlists';


const addCardList = (list: CardListData, board: BoardData) => {
  const cloneData = cloneDeep(board);
  cloneData.lists.push(list);
  return cloneData;
};

const removeCardList = (list: CardListData, board: BoardData) => {
  const cloneData = cloneDeep(board);
  const listIndex = cloneData.lists.findIndex(l => l._id === list._id);
  if (listIndex === -1) throw new Error('Removable cardlist was not in board');
  cloneData.lists.splice(listIndex, 1);
  return cloneData;
};

const updateListInBoard = (list: CardListData, board: BoardData) => {
  const cloneData = cloneDeep(board);
  const listIndex = cloneData.lists.findIndex(l => l._id === list._id);
  if (listIndex === -1) throw new Error('Updatable cardlist was not in board');
  cloneData.lists.splice(listIndex, 1, list);
  return cloneData;
};

export default { addCardList, removeCardList, updateListInBoard };

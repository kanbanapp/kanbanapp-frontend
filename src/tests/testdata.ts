import { DropResult } from 'react-beautiful-dnd';
import { BoardData } from '../types/kanban/boards';
import { CardListData } from '../types/kanban/cardlists';
import { CardData } from '../types/kanban/cards';
import { User } from '../types/kanban/users';

export const testCards: CardData[] = [
  {
    _id: 'aaedfasasdf',
    name: 'Test card 1',
    description: 'This is the first test card',
    users: ['mattiId'],
    tags: [],
  },
  {
    _id: 'gsdgf sdf gs ',
    name: 'Test card 2',
    description: 'This is the second test card',
    users: ['mattiId'],
    tags: [],
  }, {
    _id: 'asfdgsdfgsfffff',
    name: 'Test card 3',
    description: 'This is the third test card',
    users: ['mattiId'],
    tags: [],
  }, {
    _id: 'aaedfasasafadsfdf',
    name: 'Test card 4',
    description: 'This is the fourth test card',
    users: ['mattiId'],
    tags: [],
  }, {
    _id: 'aaedfasasdsaasfgdsfgfghhfmgjf',
    name: 'Test card 1',
    description: 'This is the fifth test card',
    users: ['mattiId'],
    tags: [],
  },
];

export const testCardLists: CardListData[] = [
  {
    _id: 'hfhagagasgadfasfa',
    name: 'Test List 1',
    cards: [testCards[0]],
  },
  {
    _id: 'hfadfasdfhagagasg',
    name: 'Test List 2',
    cards: [testCards[1], testCards[3]],
  },
  {
    _id: 'hfhagghghfagasg',
    name: 'Test List 3',
    cards: [testCards[1], testCards[2], testCards[3]],
  },
];


export const testBoards: BoardData[] = [
  {
    _id: 'aöslkdfjkjasdfkjlöafds',
    name: 'Board 1',
    description: 'This is the first test board',
    users: ['mattiId'], // User ids
    lists: [testCardLists[1]],
  },
  {
    _id: 'aöslkdfjkjasdfkzfgsdhsfhsdjlöafds',
    name: 'Board 2',
    description: 'This is the second test board',
    users: ['mattiId'], // User ids
    lists: [testCardLists[0], testCardLists[1]],
  },
  {
    _id: 'aöslaaadfgsdfhggjhkdfjkjasdfkjlöafds',
    name: 'Board 3',
    description: 'This is the third test board',
    users: ['mattiId'], // User ids
    lists: [testCardLists[2]],
  },
];

export const testUsers: User[] = [
  {
    _id: 'mattiId',
    username: 'Matti',
    passwordhash: 'passwordhash',
    boards: ['aöslkdfjkjasdfkjlöafds'],
    settings: [],
  },
];

export const mockCardDropResult: DropResult = {
  reason: 'DROP',
  type: 'CARD',
  source: { index: 0, droppableId: 'kissa' },
  destination: { index: 0, droppableId: 'koira' },
  draggableId: 'kissa',
  mode: 'FLUID',
};

export const mockCardListDropResult: DropResult = {
  reason: 'DROP',
  type: 'CARDLIST',
  source: { index: 0, droppableId: 'kissa' },
  destination: { index: 0, droppableId: 'koira' },
  draggableId: 'kissa',
  mode: 'FLUID',
};

/* eslint-disable no-underscore-dangle */
/* eslint-disable max-len */
/* eslint-disable no-undef */
import { DropResult } from 'react-beautiful-dnd';
import boardUtils from '../services/kanbanlogic/boardUtils';
import cardListUtils from '../services/kanbanlogic/cardListUtils';
import cardUtils from '../services/kanbanlogic/cardUtils';
import handleDrops from '../services/kanbanlogic/dndUtils';
import {
  mockCardDropResult, mockCardListDropResult, testBoards, testCardLists, testCards,
} from './testdata';

describe('Board function', () => {

  test('addBoard should return copy of boardList that includes new board', () => {
    expect(boardUtils.addBoard(testBoards[0], [])).toStrictEqual([testBoards[0]]);
  });

  test('removeBoard should return copy of boardList with correct board removed or throw', () => {
    expect(boardUtils.removeBoard(testBoards[0], [testBoards[0], testBoards[1]])).toStrictEqual([testBoards[1]]);
    expect(() => boardUtils.removeBoard(testBoards[0], [])).toThrow('Removable board did not exist in board list.');
  });

  test('updateBoard should return copy of boardList that has updated the correct board or throw', () => {
    const ub = { ...testBoards[0] };
    ub.name = 'Cat';
    expect(boardUtils.updateBoard(ub, [testBoards[0], testBoards[1]])).toStrictEqual([ub, testBoards[1]]);
    expect(() => boardUtils.updateBoard(testBoards[0], [testBoards[1]])).toThrow('Updatable board did not exist in board list.');
  });

});

describe('Cardlist function', () => {

  test('addCardList should return copy of board that includes new card list', () => {
    const testList = testCardLists[2];
    expect(cardListUtils.addCardList(testList, testBoards[0])).toStrictEqual({ ...testBoards[0], lists: [...testBoards[0].lists, testList] });
  });

  test('removeCardList should return copy of board with correct cardlist removed or throw', () => {
    expect(cardListUtils.removeCardList(testCardLists[1], testBoards[0])).toStrictEqual({ ...testBoards[0], lists: [] });
    expect(() => cardListUtils.removeCardList(testCardLists[0], testBoards[0])).toThrow('Removable cardlist was not in board');
  });

  test('updateListInBoard should return copy of boardList that has updated the correct card list or throw', () => {
    const ul = { ...testCardLists[1] };
    ul.name = 'Cat';
    expect(cardListUtils.updateListInBoard(ul, testBoards[0])).toStrictEqual({ ...testBoards[0], lists: [ul] });
    expect(() => cardListUtils.updateListInBoard(ul, testBoards[2])).toThrow('Updatable cardlist was not in board');
  });

});

describe('Card function', () => {

  test('addCardToList should return copy of list that includes new card', () => {
    const testList = testCardLists[2];
    const testCard = testCards[0];
    expect(cardUtils.addCardToList(testCard, testList)).toStrictEqual({ ...testList, cards: [...testList.cards, testCard] });
  });

  test('removeCardFromList should return copy of list with correct card removed or throw', () => {
    expect(cardUtils.removeCardFromList(testCards[1], testCardLists[1])).toStrictEqual({ ...testCardLists[1], cards: [testCards[3]] });
    expect(() => cardUtils.removeCardFromList(testCards[1], testCardLists[0])).toThrow('Removable card did not exist in list.');
  });

  test('updateCardInList should return copy of list that has updated the correct card or throw', () => {
    const uc = { ...testCards[0] };
    uc.name = 'Cat';
    expect(cardUtils.updateCardInList(uc, testCardLists[0])).toStrictEqual({ ...testCardLists[0], cards: [uc] });
    expect(() => cardUtils.updateCardInList(uc, testCardLists[1])).toThrow('Updatable card did not exist in list.');
  });

});

describe('Drag and drop function', () => {

  test('handleDrops should handle card drop correctly', () => {
    const testBoard = testBoards[1];

    // Drag lone card to middle of another list
    const drop = {
      ...mockCardDropResult,
      source: {
        index: 0,
        droppableId: testBoard.lists[0]._id,
      },
      destination: {
        index: 1,
        droppableId: testBoard.lists[1]._id,
      },
      draggableId: testBoard.lists[0].cards[0]._id,
    };

    const result = handleDrops(drop, testBoard);

    expect(result).toStrictEqual({
      ...testBoard,
      lists: [
        { ...testBoard.lists[0], cards: [] },
        { ...testBoard.lists[1], cards: [testCards[1], testCards[0], testCards[3]] },
      ],
    });
  });

  test('handleDrops should handle card list drop correctly', () => {
    const testBoard = testBoards[1];

    // Swap card list places
    const drop = {
      ...mockCardListDropResult,
      source: {
        index: 1,
        droppableId: testBoard._id,
      },
      destination: {
        index: 0,
        droppableId: testBoard._id,
      },
      draggableId: testBoard.lists[1]._id,
    };

    const result = handleDrops(drop, testBoard);
    expect(result).toStrictEqual({
      ...testBoard,
      lists: [
        testCardLists[1],
        testCardLists[0],
      ],
    });
  });

});

/* eslint-disable no-underscore-dangle */
/* eslint-disable no-debugger */
import React, { useEffect } from 'react';
import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';
import Board from './components/Board';
import CardInfoView from './components/CardInfoView';
import BoardMenu from './components/BoardMenu';
import AppView from './types/appview';
import TopBar from './components/TopBar';
import BoardViewBar from './components/BoardViewBar';
import { RootReducerState } from './types/redux/reducers';
import WelcomeView from './components/WelcomeView';
import { changeView } from './redux/views/viewActions';
import AnnouncementView from './components/AnnouncementView';
import { initBoards } from './redux/boards/boardActions';
import { loginExistingUser } from './redux/users/userActions';

const AppArea = styled.div`

  `;

const App = () => {

  const allBoards = useSelector((state: RootReducerState) => state.boardstate.boards);
  const currentView = useSelector((state: RootReducerState) => state.viewstate.view);
  const currentBoard = useSelector((state: RootReducerState) => state.viewstate.currentBoard);
  const currentCard = useSelector((state: RootReducerState) => state.viewstate.currentCard);
  const currentCardList = currentBoard?.lists.find(cl => currentCard && cl.cards.includes(currentCard));
  const currentUser = useSelector((state: RootReducerState) => state.userstate.user);
  const error = useSelector((state: RootReducerState) => state.errorstate.error);
  const dispatch = useDispatch();

  // Try login with possible user token in local storage
  useEffect(() => {
    console.log('Try existing user login');
    if ((!currentUser?.usertoken) && localStorage.getItem('userToken')) dispatch(loginExistingUser());
  }, [currentUser, dispatch]);

  // Fetch boards and go to board menu on login
  useEffect(() => {
    if (currentUser && currentUser.usertoken) {
      dispatch(initBoards());
      dispatch(changeView(AppView.BOARDMENU));
    } else {
      dispatch(changeView(AppView.WELCOMEVIEW));
    }
  }, [currentUser, dispatch]);

  const viewSelector = (selectedView: AppView): JSX.Element|undefined => {

    switch (currentView) {

      case AppView.BOARDMENU:
        return <BoardMenu boards={allBoards} />;

      case AppView.BOARDVIEW:
        return (
          <>
            { currentCard && currentCardList ? <CardInfoView card={currentCard} cardList={currentCardList} /> : '' }
            { currentBoard ? <BoardViewBar board={currentBoard} /> : ''}
            { currentBoard ? <Board boardData={currentBoard} /> : 'No board data' }
          </>
        );

      case AppView.WELCOMEVIEW:
        return <WelcomeView />;

      default: return undefined;

    }
  };

  return (
    // eslint-disable-next-line jsx-a11y/no-noninteractive-element-interactions
    <AppArea
      onKeyPress={() => {}}
      role="region"
    >
      {error ? <AnnouncementView announcement={error} /> : ''}
      <TopBar />
      {viewSelector(currentView)}
    </AppArea>
  );
};

export default App;

import { CardData } from './cards';


export type CardListData = {
    _id: string, // Uuid or mongo generated
    name: string,
    cards: CardData[]
}

export type CardListDTO = {
    _id: string, // Uuid or mongo generated
    name: string,
    cards: string[] // Card ids
}

export type NewCardListData = Omit<CardListDTO, '_id'>

/* eslint-disable no-underscore-dangle */

import { AdditionalData } from './additionaldata';
import { CardListData } from './cardlists';

export type BoardData = {
    _id: string, // Uuid or mongo generated
    name: string,
    description: string,
    users: string[], // User ids
    lists: CardListData[]
    additionalData?: AdditionalData[]
}

export type NewBoardData = Omit<BoardData, '_id'>



import React, { FunctionComponent, useState } from 'react';
import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';
import AppLogo from '../images/KanbanAppLogo.svg';
import { RootReducerState } from '../types/redux/reducers';
import { User } from '../types/kanban/users';
import { loginUser, signupUser, logoutUser } from '../redux/users/userActions';

interface MenuBarProps {
}

const MenuBarContainer = styled.div`
  display: flex;
  flex-direction: row;
  background-color: #577399;
  overflow: hidden;
  height: 3.5rem;
  position: fixed;
  width: 100%
`;

const Logo = styled.img`
  margin: .5rem; 
`;

const LoginForm = styled.form`
  align-self: center;
  margin-left: auto;
  margin-right: 1rem;
`;

const LoginInput = styled.input`
  margin-right: .5rem;
`;

const UserText = styled.p`
  margin-left: auto;
  margin-right: .5rem;
`;

const LogoutButton = styled.button`
  margin: .5rem;
  border-radius: .5rem;
  border-style: none;
`;

const LoginMenuButton = styled.button`
  margin: .3rem;
  border-radius: .5rem;
  border-style: none;
`;

const TopBar: FunctionComponent<MenuBarProps> = (props) => {

  const user = useSelector((state: RootReducerState) => state.userstate.user);
  const dispatch = useDispatch();
  const [username, setUserName] = useState('');
  const [password, setPassword] = useState('');

  const onUserNameChange = (e: React.ChangeEvent<HTMLInputElement>) => setUserName(e.target.value);

  const onPasswordChange = (e: React.ChangeEvent<HTMLInputElement>) => setPassword(e.target.value);

  const onLogin = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    dispatch(loginUser(username, password));
    setUserName('');
    setPassword('');
  };

  const onRegister = (e: React.MouseEvent) => {
    e.preventDefault();
    dispatch(signupUser(username, password));
  };

  const onLogout = (e: React.MouseEvent) => {
    e.preventDefault();
    dispatch(logoutUser());
  };

  const loginForm = (
    <LoginForm onSubmit={onLogin}>
      <LoginInput onChange={onUserNameChange} value={username} placeholder="Username" />
      <LoginInput onChange={onPasswordChange} value={password} placeholder="Password" type="password" />
      <LoginMenuButton type="submit">Login</LoginMenuButton>
      <LoginMenuButton type="button" onClick={onRegister}>Register</LoginMenuButton>
    </LoginForm>
  );

  const userView = (u: User) => (
    <>
      <UserText>
        {`${u.username} logged in`}
      </UserText>
      <LogoutButton type="button" onClick={onLogout}>Logout</LogoutButton>
    </>
  );




  return (
    <MenuBarContainer>
      <Logo src={AppLogo} />
      {user && user.username !== 'Guest' ? userView(user) : loginForm}
    </MenuBarContainer>
  );
};

export default TopBar;

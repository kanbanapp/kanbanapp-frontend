/* eslint-disable no-underscore-dangle */
import React, {
  ChangeEvent, FunctionComponent, useRef, useState,
} from 'react';
import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';
import AppView from '../types/appview';
import Editable from './Editable';
import DropdownMenu from './DropdownMenu';
import { BoardData } from '../types/kanban/boards';
import { deleteBoard, updateBoard } from '../redux/boards/boardActions';
import { changeView } from '../redux/views/viewActions';
import userService from '../services/comms/userService';
import { RootReducerState } from '../types/redux/reducers';

const BoardViewBarContainer = styled.div`
  display: flex;
  background-color: rgba(173, 173, 173, 0.7);
  padding: .5rem;
  z-index: 1;
  position: fixed;
  top: 3.5rem;
  width: 100%
`;

const BoardHeaderText = styled.h1`
  margin: .5rem;
`;

const MenuButton = styled.button`
  margin-right: .5rem;
  border-radius: .5rem;
  border-style: none;
`;

const DropMenuButton = styled.button`
  height: 100%;
  border-radius: .5rem;
  border-style: none;
`;

const BoardNameInput = styled.input`
margin: .5rem;
min-height: 2rem;
display: block;
`;

const DropDownButton = styled.button`
width: 100%;
height: 2rem;
border: none;
background-color: white;

&:hover {
  background-color: lightgray;
}
`;

interface BoardViewBarProps {
  board: BoardData,
}

const BoardViewBar: FunctionComponent<BoardViewBarProps> = (props) => {

  const dispatch = useDispatch();
  const user = useSelector((state: RootReducerState) => { return (state.userstate.user); });
  const [boardName, setBoardName] = useState(props.board.name);
  const nameref: React.MutableRefObject<any> = useRef<any>();
  const fileref: React.MutableRefObject<any> = useRef<any>();
  const imageUrl = props.board.additionalData?.find(o => Object.keys(o).includes('image'))?.image;

  const onBoardNameChange = (e: ChangeEvent<HTMLInputElement>) => setBoardName(e.target.value);

  const onBoardNameBlur = (e: React.FocusEvent<HTMLInputElement>) => {
    if (boardName === '') {
      setBoardName(props.board.name);
    } else {
      dispatch(updateBoard({ ...props.board, name: boardName }));
    }
  };

  const onBoardDelete = () => {
    dispatch(changeView(AppView.BOARDMENU));
    dispatch(deleteBoard(props.board));
  };

  const onMenuButtonClick = () => {
    dispatch(changeView(AppView.BOARDMENU));
  };

  const onFileChange = async (e: React.ChangeEvent<HTMLInputElement>) => {
    e.stopPropagation();
    e.preventDefault();
    console.log('FileChange');

    const file = e.target.files?.item(0);
    e.target.value = '';
    if (file && user.usertoken) {

      const formData = new FormData();
      formData.append('file', file);
      await userService.addImageOnline(user, user.usertoken, formData);

      const path = `${process.env.REACT_APP_SERVERURL}/users/${user._id}/images/${file.name}`;
      const oldPath = imageUrl;

      dispatch(updateBoard({ ...props.board, additionalData: [{ image: path }] }));

      if (oldPath && path !== oldPath) await userService.deleteImageOnline(user.usertoken, oldPath);
    }
  };

  const onChangeBackground = (e: React.MouseEvent) => {
    e.stopPropagation();
    fileref.current.click();
  };

  const onClearBackground = async (e: React.MouseEvent) => {
    e.preventDefault();

    if (!imageUrl) return;

    const updatedBoard = { ...props.board, additionalData: [] };
    dispatch(updateBoard(updatedBoard));

    if (user.usertoken) await userService.deleteImageOnline(user.usertoken, imageUrl);
  };


  return (
    <BoardViewBarContainer>
      <BoardHeaderText>
        <Editable placeholder="Board name" text={boardName} childRef={nameref}>
          <BoardNameInput
            ref={nameref}
            type="text"
            name="Board name"
            placeholder="Board name"
            value={boardName}
            onChange={onBoardNameChange}
            onBlur={onBoardNameBlur}
          />
        </Editable>
      </BoardHeaderText>
      <MenuButton onClick={onMenuButtonClick}>Board selection menu</MenuButton>
      <DropdownMenu>
        <DropMenuButton>Board options</DropMenuButton>
        <DropDownButton onClick={onBoardDelete}>Delete</DropDownButton>
        <DropDownButton onClick={onChangeBackground}>Change background</DropDownButton>
        {imageUrl && <DropDownButton onClick={onClearBackground}>Clear background</DropDownButton>}
      </DropdownMenu>
      <input
        type="file"
        style={{ display: 'none' }}
        ref={fileref}
        onChange={onFileChange}
        accept="image/png, image/jpeg"
      />
    </BoardViewBarContainer>
  );
};

export default BoardViewBar;

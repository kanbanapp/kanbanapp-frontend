import React, { FunctionComponent } from 'react';
import styled from 'styled-components';
import AppLogo from '../images/KanbanAppLogo.svg';

interface WelcomeViewProp {
}

const WelcomeViewContainer = styled.div`
    min-height: 80vh;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
`;

const LogoImg = styled.img`
    width: 100px;
    height: 100px;
`;

const WelcomeView: FunctionComponent<WelcomeViewProp> = (props) => {
  return (
    <WelcomeViewContainer>
      <LogoImg src={AppLogo} alt="KanbanApp logo" />
      <h1>Welcome to KanbanApp!</h1>
      <p>Please log in or register using the form in the top right corner of the screen.</p>
    </WelcomeViewContainer>
  );
};

export default WelcomeView;

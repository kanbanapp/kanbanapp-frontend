/* eslint-disable jsx-a11y/click-events-have-key-events */
import React, { FunctionComponent, useEffect, useState } from 'react';
import styled from 'styled-components';

interface EditableProps {
    placeholder: string
    text: string
    childRef: React.MutableRefObject<any>
}

const EditableContainer = styled.div`
`;

const ContentContainer = styled.div`
  word-wrap: break-word;
`;

const Editable: FunctionComponent<EditableProps> = (props) => {
  const {
    placeholder, text, children, childRef,
  } = props;
  const [editMode, setEditMode] = useState(false);

  useEffect(() => {
    if (childRef && childRef.current && editMode) childRef.current.focus();
  }, [editMode, childRef]);

  return (
    <EditableContainer>
      {
            editMode
              ? (<EditableContainer onBlur={() => setEditMode(false)}>{ children }</EditableContainer>)
              // eslint-disable-next-line jsx-a11y/no-static-element-interactions
              : (<EditableContainer onClick={() => setEditMode(true)}><ContentContainer>{text || placeholder || 'No content'}</ContentContainer></EditableContainer>)
        }
    </EditableContainer>
  );
};

export default Editable;

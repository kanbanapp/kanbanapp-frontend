import React, { FunctionComponent } from 'react';
import styled from 'styled-components';

interface AnnouncementViewProps {
    announcement: string
}

const AnnouncementContainer = styled.div`
    background-color: red;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
`;

const AnnouncementView: FunctionComponent<AnnouncementViewProps> = (props) => {
  return (
    <AnnouncementContainer>
      {props.announcement}
    </AnnouncementContainer>
  );
};

export default AnnouncementView;

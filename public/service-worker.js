/* eslint-disable no-underscore-dangle */
/* eslint-disable no-return-assign */
/* eslint-disable no-restricted-globals */


// IndexedDB setup
const databaseName = 'KanbanApp';
const boardStoreName = 'Boards';
let dbVersion = 5;

let database;

const openDb = async (callback) => {
  const request = self.indexedDB.open(databaseName, dbVersion);
  dbVersion += 1;
  request.onerror = (event) => console.warn(`IDB error ${event.target.error}`);
  request.onupgradeneeded = (event) => {
    database = event.target.result;
    if (!database.objectStoreNames.contains(boardStoreName)) {
      database.createObjectStore(boardStoreName, { keyPath: 'boardId', autoIncrement: true });
    }
  };
  request.onsuccess = (event) => {
    database = event.target.result;
    if (callback) callback();
  };
};

const addToBoardStore = (key, value) => {
  return new Promise((resolve, reject) => {
    const transaction = database.transaction(boardStoreName, 'readwrite');
    const store = transaction.objectStore(boardStoreName);
    const request = store.put({ key, value });
    request.onSuccess = () => console.log('Add request to IDB successful');
    request.onError = () => reject(new Error('Add request to IDB failed'));
    transaction.onSuccess = () => resolve(value);
    transaction.onError = () => reject(new Error('Trans error'));
  });
};

const getOneFromBoardStore = (key) => {
  return new Promise((resolve, reject) => {
    const transaction = database.transaction(boardStoreName, 'readwrite');
    const store = transaction.objectStore(boardStoreName);
    const request = store.get(key);
    request.onSuccess = (event) => {
      resolve(event.target.result.value);
    };
    request.onError = () => reject(new Error('Get req error'));
    transaction.onSuccess = () => console.log('Get trans success');
    transaction.onError = () => reject(new Error('Get trans error'));
  });
};

const getAllFromBoardStore = () => {
  return new Promise((resolve, reject) => {
    const transaction = database.transaction(boardStoreName, 'readwrite');
    const store = transaction.objectStore(boardStoreName);
    const request = store.getAll();
    request.onSuccess = (event) => {
      resolve(event.target.result.value);
    };
    request.onError = () => reject(new Error('Get req error'));
    transaction.onSuccess = () => console.log('Get trans success');
    transaction.onError = () => reject(new Error('Get trans error'));
  });
};

const handleBoardPost = async (event) => {
  const reqForBody = event.request.clone();
  const reqForFetch = event.request.clone();
  const board = await reqForBody.json();
  addToBoardStore(board._id, board);
  const serverResponse = await fetch(reqForFetch);
  if (serverResponse.ok) {
    return serverResponse;
  }
  console.log('Server error.');
  return new Response(board);
};

const handleBoardGet = async (event) => {
  const reqForFetch = event.request.clone();
  const serverResponse = await fetch(reqForFetch);
  const isGetAllRequest = !reqForFetch.url.split('/').length === 5;
  console.log(reqForFetch.url);
  if (serverResponse.ok) {
    const data = await serverResponse.json();
    console.log(data);
    data.forEach(b => addToBoardStore(b._id, b));
  }
  if (isGetAllRequest) {
    const boards = await getAllFromBoardStore();
    return new Response(boards);
  }
  const id = reqForFetch.url.split('/')[3];
  console.log(id);
  const board = await getOneFromBoardStore(id);
  if (board) return (new Response(board));
  return (new Response(new Error('Board not found')));
};

const onFetch = async (event) => {
  console.log('Fetch event');

  const { url, method } = event.request;
  console.log(url);
  console.log(method);
  console.log(`Database${database}`);

  if (url.includes('board')) {
    if (method === 'POST') {
      event.respondWith(handleBoardPost(event));
    }

    if (method === 'GET') {
      event.respondWith(handleBoardGet(event));
    }

    // if(method === 'PUT'){

    // }

    // if(method === 'DELETE'){

    // }
  }
};


self.addEventListener('activate', (event) => {
  // Message to simply show the lifecycle flow
  console.log('[activate] Activating service worker!');
  // Claim the service work for this client, forcing `controllerchange` event
  console.log('[activate] Claiming this service worker!');
  event.waitUntil(self.clients.claim());

  openDb(() => {
    console.log('Database open');
  });
});

self.addEventListener('fetch', async (fetchEvent) => {
  await onFetch(fetchEvent);
});
